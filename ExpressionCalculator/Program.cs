﻿using System;
using ExpressionCalculator.Engine.Exceptions;
using ExpressionCalculator.Engine.Utility;

namespace ExpressionCalculator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                CalculateExpression();
            }
        }

        private static void CalculateExpression()
        {
            Console.WriteLine("Enter expression to calculate:");
            var expression = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(expression))
            {
                Console.WriteLine("Undefined or unknown expression. Try again.");
                return;
            }

            try
            {
                var root = ParserUtility.Parse(expression);
                var result = root.Evaluate();
                Console.WriteLine($"Result: {result}");
            }
            catch(SyntaxException ex)
            {
                Console.WriteLine($"{ex.Message}. Try again.");
            }
        }
    }
}
