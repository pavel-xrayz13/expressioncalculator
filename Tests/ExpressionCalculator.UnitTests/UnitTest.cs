﻿using System.IO;
using ExpressionCalculator.Engine;
using ExpressionCalculator.Engine.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionCalculator.UnitTests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TokenizerTest()
        {
            const string testString = "10 + 20 - 30.123";
            var t = new Tokenizer(new StringReader(testString));

            // "10"
            Assert.AreEqual(t.Token, Token.Number);
            Assert.AreEqual(t.Number, 10);
            t.NextToken();

            // "+"
            Assert.AreEqual(t.Token, Token.Add);
            t.NextToken();

            // "20"
            Assert.AreEqual(t.Token, Token.Number);
            Assert.AreEqual(t.Number, 20);
            t.NextToken();

            // "-"
            Assert.AreEqual(t.Token, Token.Subtract);
            t.NextToken();

            // "30.123"
            Assert.AreEqual(t.Token, Token.Number);
            Assert.AreEqual(t.Number, 30.123);
            t.NextToken();
        }

        [TestMethod]
        public void AddSubtractTest()
        {
            Assert.AreEqual(ParserUtility.Parse("10 + 20").Evaluate(), 30);
            Assert.AreEqual(ParserUtility.Parse("10 - 20").Evaluate(), -10);
            Assert.AreEqual(ParserUtility.Parse("10 + 20 - 40 + 100").Evaluate(), 90);
        }

        [TestMethod]
        public void UnaryTest()
        {
            Assert.AreEqual(ParserUtility.Parse("-10").Evaluate(), -10);
            Assert.AreEqual(ParserUtility.Parse("+10").Evaluate(), 10);
            Assert.AreEqual(ParserUtility.Parse("--10").Evaluate(), 10);
            Assert.AreEqual(ParserUtility.Parse("--++-+-10").Evaluate(), 10);
            Assert.AreEqual(ParserUtility.Parse("10 + -20 - +30").Evaluate(), -40);
        }

        [TestMethod]
        public void MultiplyDivideTest()
        {
            Assert.AreEqual(ParserUtility.Parse("10 * 20").Evaluate(), 200);
            Assert.AreEqual(ParserUtility.Parse("10 / 20").Evaluate(), 0.5);
            Assert.AreEqual(ParserUtility.Parse("10 * 20 / 50").Evaluate(), 4);
        }

        [TestMethod]
        public void OrderOfOperation()
        {
            Assert.AreEqual(ParserUtility.Parse("10 + 20 * 30").Evaluate(), 610);
            Assert.AreEqual(ParserUtility.Parse("(10 + 20) * 30").Evaluate(), 900);
            Assert.AreEqual(ParserUtility.Parse("-(10 + 20) * 30").Evaluate(), -900);
            Assert.AreEqual(ParserUtility.Parse("-((10 + 20) * 5) * 30").Evaluate(), -4500);
        }
    }
}
