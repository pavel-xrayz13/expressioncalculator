﻿namespace ExpressionCalculator.Engine
{
    public enum Token
    {
        Eof,
        Add,
        Subtract,
        Multiply,
        Divide,
        OpenParens,
        CloseParens,
        Comma,
        Number
    }
}
