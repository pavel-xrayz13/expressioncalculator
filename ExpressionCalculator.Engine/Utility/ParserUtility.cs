﻿using System.IO;

namespace ExpressionCalculator.Engine.Utility
{
    public static class ParserUtility
    {
        public static Node Parse(string expression)
        {
            return Parse(new Tokenizer(new StringReader(expression)));
        }

        public static Node Parse(Tokenizer tokenizer)
        {
            var parser = new Parser(tokenizer);
            return parser.Parse();
        }
    }
}
