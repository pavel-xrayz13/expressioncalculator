﻿using System;

namespace ExpressionCalculator.Engine.Nodes
{
    /// <summary>
    /// Node that processes expressions of type A+B
    /// </summary>
    public class BinaryNode : Node
    {
        private readonly Node _leftNode;
        private readonly Node _rightNode;
        private readonly Func<double, double, double> _operator;

        public BinaryNode(Node leftNode, Node rightNode, Func<double, double, double> @operator)
        {
            _leftNode = leftNode ?? throw new ArgumentNullException(nameof(leftNode));
            _rightNode = rightNode ?? throw new ArgumentNullException(nameof(rightNode));
            _operator = @operator ?? throw new ArgumentNullException(nameof(@operator));
        }

        public override double Evaluate()
        {
            var lhsVal = _leftNode.Evaluate();
            var rhsVal = _rightNode.Evaluate();
            return _operator(lhsVal, rhsVal);
        }
    }
}
