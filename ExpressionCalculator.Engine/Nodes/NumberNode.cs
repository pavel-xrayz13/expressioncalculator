﻿namespace ExpressionCalculator.Engine.Nodes
{
    /// <summary>
    /// Node that processes expressions of type A
    /// </summary>
    public class NumberNode : Node
    {
        private readonly double _number;

        public NumberNode(double number)
        {
            _number = number;
        }

        public override double Evaluate() => _number;
    }
}
