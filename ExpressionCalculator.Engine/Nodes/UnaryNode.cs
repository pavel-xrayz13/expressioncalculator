﻿using System;

namespace ExpressionCalculator.Engine.Nodes
{
    /// <summary>
    /// Node that processes expressions of type -A
    /// </summary>
    public class UnaryNode : Node
    {
        private readonly Node _rightNode;
        private readonly Func<double, double> _operator;

        public UnaryNode(Node rhs, Func<double, double> @operator)
        {
            _rightNode = rhs ?? throw new ArgumentNullException(nameof(rhs));
            _operator = @operator ?? throw new ArgumentNullException(nameof(@operator));
        }

        public override double Evaluate()
        {
            var rhsVal = _rightNode.Evaluate();
            return _operator(rhsVal);
        }
    }
}
