﻿using System;

namespace ExpressionCalculator.Engine.Exceptions
{
    /// <summary>
    /// Uses for syntax exceptions only.
    /// </summary>
    public class SyntaxException : Exception
    {
        public SyntaxException(string message) : base(message)
        {
        }
    }
}
