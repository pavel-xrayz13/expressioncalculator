﻿using System;
using ExpressionCalculator.Engine.Exceptions;
using ExpressionCalculator.Engine.Nodes;

namespace ExpressionCalculator.Engine
{
    /// <summary>
    /// Expression parser implementation.
    /// </summary>
    public class Parser
    {
        private readonly Tokenizer _tokenizer;

        public Parser(Tokenizer tokenizer)
        {
            _tokenizer = tokenizer;
        }

        public Node Parse()
        {
            var expr = ParseAddSubtract(); // start as add and subtract
            if (_tokenizer.Token != Token.Eof) // check everything was consumed
                throw new SyntaxException("Unexpected characters at end of expression");

            return expr;
        }

        private Node ParseAddSubtract()
        {
            // parse the left hand side
            var leftNode = ParseMultiplyDivide();

            while (true)
            {
                Func<double, double, double> op = null;

                if (_tokenizer.Token == Token.Add)
                {
                    op = (a, b) => a + b;
                }
                else if (_tokenizer.Token == Token.Subtract)
                {
                    op = (a, b) => a - b;
                }

                
                if (op == null)
                    return leftNode; // no binary operator found

                _tokenizer.NextToken(); // skip the operator

                // parse the right hand side of the expression
                var rightNode = ParseMultiplyDivide();

                leftNode = new BinaryNode(leftNode, rightNode, op);
            }
        }

        private Node ParseMultiplyDivide()
        {
            // parse the left hand side
            var leftNode = ParseUnary();

            while (true)
            {
                Func<double, double, double> op = null;

                if (_tokenizer.Token == Token.Multiply)
                {
                    op = (a, b) => a * b;
                }
                else if (_tokenizer.Token == Token.Divide)
                {
                    op = (a, b) => a / b;
                }

                if (op == null)
                    return leftNode; // no binary operator found

                _tokenizer.NextToken(); // skip the operator

                // parse the right hand side of the expression
                var rightNode = ParseUnary();

                leftNode = new BinaryNode(leftNode, rightNode, op);
            }
        }

        private Node ParseUnary()
        {
            while (true)
            {
                if (_tokenizer.Token == Token.Add)
                {
                    // positive operator is a noop in our case, so just skip it

                    _tokenizer.NextToken();
                    continue;
                }

                if (_tokenizer.Token == Token.Subtract) // negative operator
                {
                    _tokenizer.NextToken();

                    // parse right node 
                    // note this makes recursion to self to support negative of a negative
                    var rightNode = ParseUnary();

                    return new UnaryNode(rightNode, (a) => -a);
                }

                // no positive/negative operator so pass-through
                return ParseLeaf();
            }
        }

        private Node ParseLeaf()
        {
            if (_tokenizer.Token == Token.Number)  // check if this is a number
            {
                var node = new NumberNode(_tokenizer.Number);
                _tokenizer.NextToken();
                return node;
            }

            if (_tokenizer.Token == Token.OpenParens) // check for parenthesis
            {
                // skip '('
                _tokenizer.NextToken();

                // parse a top-level expression inside
                var node = ParseAddSubtract();

                // check and skip ')'
                if (_tokenizer.Token != Token.CloseParens)
                    throw new SyntaxException("Missing close parenthesis");

                _tokenizer.NextToken();
                return node;
            }

            throw new SyntaxException($"Unexpected token: {_tokenizer.Token}");
        }
    }
}
