﻿namespace ExpressionCalculator.Engine
{
    public abstract class Node
    {
        public abstract double Evaluate();
    }
}
