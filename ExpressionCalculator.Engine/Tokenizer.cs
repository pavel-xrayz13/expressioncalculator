﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace ExpressionCalculator.Engine
{
    /// <summary>
    /// Simple math expression tokenizer.
    /// </summary>
    public class Tokenizer
    {
        private readonly TextReader _reader;
        private char _currentChar;

        public Tokenizer(TextReader reader)
        {
            _reader = reader ?? throw new ArgumentNullException(nameof(reader));

            NextChar();
            NextToken();
        }

        public Token Token { get; private set; }

        public double Number { get; private set; }

        /// <summary>
        /// Read the next character from the input stream
        /// and store it in _currentChar, or load '\0' if EOF
        /// </summary>
        private void NextChar()
        {
            var ch = _reader.Read();
            _currentChar = ch < 0 ? '\0' : (char)ch;
        }

        /// <summary>
        /// Read the next token from the input stream
        /// </summary>
        public void NextToken()
        {
            // Skip whitespace
            while (char.IsWhiteSpace(_currentChar))
            {
                NextChar();
            }

            // Special characters
            switch (_currentChar)
            {
                case '\0':
                    Token = Token.Eof;
                    return;

                case '+':
                    NextChar();
                    Token = Token.Add;
                    return;

                case '-':
                    NextChar();
                    Token = Token.Subtract;
                    return;

                case '*':
                    NextChar();
                    Token = Token.Multiply;
                    return;

                case '/':
                    NextChar();
                    Token = Token.Divide;
                    return;

                case '(':
                    NextChar();
                    Token = Token.OpenParens;
                    return;

                case ')':
                    NextChar();
                    Token = Token.CloseParens;
                    return;

                case ',':
                    NextChar();
                    Token = Token.Comma;
                    return;
            }

            if (char.IsDigit(_currentChar) || _currentChar == '.') // Number?
            {
                // Capture digits/decimal point
                var sb = new StringBuilder();
                var hasDecimalPoint = false;

                while (char.IsDigit(_currentChar) || !hasDecimalPoint && _currentChar == '.')
                {
                    sb.Append(_currentChar);
                    hasDecimalPoint = _currentChar == '.';
                    NextChar();
                }

                // Parse it
                Number = double.Parse(sb.ToString(), CultureInfo.InvariantCulture);
                Token = Token.Number;
            }
        }
    }
}
